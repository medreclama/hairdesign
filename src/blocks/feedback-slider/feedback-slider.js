const feedbacksInfo = document.querySelectorAll('.feedback-slider__info');
const feedbackSliderInfo = () => {
  feedbacksInfo.forEach((item) => {
    if (item.innerText.length > 100) {
      const itemContent = item.innerHTML;
      let showMoreButton;
      let hideButton;
      const hide = (event) => {
        if (event) event.preventDefault();
        const beginLastSpace = item.innerText.slice(0, 100).lastIndexOf(' ') - 1;
        const begin = item.innerText.slice(0, beginLastSpace + 1);
        Array.from(item.children).forEach((child) => child.remove());
        item.insertAdjacentHTML('beforeend', `<p>${begin}... <a href="#" class="feedback-item__show-more">Читать полностью</a></p>`);
        showMoreButton = item.querySelector('.feedback-item__show-more');
        // eslint-disable-next-line no-use-before-define
        showMoreButton.addEventListener('click', show);
      };
      const show = (event) => {
        event.preventDefault();
        showMoreButton.remove();
        item.firstElementChild.remove();
        item.insertAdjacentHTML('beforeend', itemContent);
        item.lastElementChild.insertAdjacentHTML('beforeend', ' <a href="#" class="feedback-item__hide">Свернуть</a>');
        hideButton = item.querySelector('.feedback-item__hide');
        hideButton.addEventListener('click', hide);
      };
      hide();
    }
  });
};

export default feedbackSliderInfo;
