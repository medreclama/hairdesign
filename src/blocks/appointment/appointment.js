const appointment = () => {
  const appointmentDiv = document.querySelector('.appointment');
  setTimeout(() => {
    appointmentDiv.classList.add('appointment--hidden');
    localStorage.setItem('appointmentDismissed', 'true');
  }, 10000);
};

export default appointment;
