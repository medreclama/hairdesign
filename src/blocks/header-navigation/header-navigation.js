function headerNavigationSublistSwitcher() {
  const hnss = Array.from(document.querySelectorAll('.header-navigation__sublist-switch'));
  hnss.forEach((item) => {
    item.addEventListener('click', (e) => {
      e.target.classList.toggle('header-navigation__sublist-switch--active');
      e.target.nextElementSibling.classList.toggle('header-navigation__sublist--active');
    });
    return item;
  });
}

export default function headerNavigation() {
  headerNavigationSublistSwitcher();
}
