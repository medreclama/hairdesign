/* global VK */
import fancybox from '@fancyapps/fancybox'; // eslint-disable-line no-unused-vars
import {
  Swiper,
  Navigation,
  Pagination,
  Thumbs,
} from 'swiper';
import './modules/jquery.countdown'; // eslint-disable-line no-unused-vars
import headerMobileSwitcher from '../blocks/header-mobile-switcher/header-mobile-switcher';
import headerNavigation from '../blocks/header-navigation/header-navigation';
import showHide from '../blocks/show-hide/show-hide';
import tabs from '../blocks/tabs/tabs';
import feedbackItem from '../blocks/feedback/feedback-item/feedback-item';
import counters from './modules/counters';
import modal from '../blocks/modal/modal';
import appointment from '../blocks/appointment/appointment';
import feedbackSliderInfo from '../blocks/feedback-slider/feedback-slider';
import video360Init from '../blocks/video-js/video-js';

headerMobileSwitcher();
headerNavigation();
tabs();
showHide();
feedbackItem();
modal();

// eslint-disable-next-line no-unused-vars
const feedbackSlider = new Swiper('.feedback-slider:not(.feedback-slider--shorts)', {
  modules: [Navigation, Pagination],
  breakpoints: {
    800: {
      spaceBetween: 30,
      slidesPerView: 2,
      slidesPerGroup: 2,
    },
  },
  spaceBetween: 0,
  slidesPerView: 1,
  slidesPerGroup: 1,
  navigation: {
    nextEl: '.feedback-slider__navigation--next',
    prevEl: '.feedback-slider__navigation--prev',
    disabledClass: 'feedback-slider__navigation--disabled',
  },
  pagination: {
    el: '.feedback-slider__pagination',
    type: 'bullets',
  },
});

const feedbackSliderShortsInit = () => {
  const feedbackSlides = document.querySelectorAll('.feedback-slider--shorts .feedback-slider__slide');
  const players = {};
  const playingStates = {};
  let currentPlayingIndex = null;

  const handleVideoStart = (index) => {
    Object.keys(players).forEach((key) => {
      if (parseInt(key, 10) !== index && players[key]) {
        players[key].pause();
        playingStates[key] = false;
      }
    });
    currentPlayingIndex = index;
    playingStates[index] = true;
  };

  feedbackSlides.forEach((slide, index) => {
    const image = slide.querySelector('.feedback-slider__image');
    const iframe = slide.querySelector('.iframe-responsive');

    image.addEventListener('click', () => {
      const iframeLayer = document.createElement('div');
      iframeLayer.classList.add('feedback-slider__layer');
      iframe.appendChild(iframeLayer);

      if (currentPlayingIndex !== null && players[currentPlayingIndex]) {
        players[currentPlayingIndex].pause();
        playingStates[currentPlayingIndex] = false;
      }

      iframe.classList.remove('feedback-slider__video--hidden');
      const videoFrame = iframe.querySelector('iframe');
      videoFrame.src = iframe.dataset.slideSrc;
      image.remove();

      if (!players[index]) {
        players[index] = VK.VideoPlayer(videoFrame);
        players[index].on('started', () => handleVideoStart(index));
        players[index].on('resumed', () => handleVideoStart(index));
        players[index].on('paused', () => {
          playingStates[index] = false;
        });
      }

      players[index].play();
      playingStates[index] = true;
      currentPlayingIndex = index;

      iframeLayer.addEventListener('click', () => {
        if (players[index]) {
          if (playingStates[index]) players[index].pause();
          else players[index].play();
          playingStates[index] = !playingStates[index];
        }
      });
    });
  });

  // eslint-disable-next-line no-unused-vars
  const feedbackSliderShorts = new Swiper('.feedback-slider--shorts', {
    modules: [Navigation, Pagination],
    spaceBetween: 30,
    slidesPerView: 1,
    breakpoints: {
      800: {
        slidesPerView: 2,
        spaceBetween: 30,
      },
      1024: {
        spaceBetween: 30,
        slidesPerView: 3,
      },
    },
    navigation: {
      nextEl: '.feedback-slider__navigation--next',
      prevEl: '.feedback-slider__navigation--prev',
      disabledClass: 'feedback-slider__navigation--disabled',
    },
    pagination: {
      el: '.feedback-slider__pagination',
      type: 'bullets',
    },
    on: {
      slideChange: () => {
        if (currentPlayingIndex !== null && players[currentPlayingIndex]) {
          players[currentPlayingIndex].pause();
          playingStates[currentPlayingIndex] = false;
          currentPlayingIndex = null;
        }
      },
    },
  });
};

feedbackSliderShortsInit();
// eslint-disable-next-line no-unused-vars
const discountsSlider = new Swiper('.discounts-slider__slider', {
  modules: [Navigation, Pagination],
  navigation: {
    nextEl: '.discounts-slider__navigation--next',
    prevEl: '.discounts-slider__navigation--prev',
    disabledClass: 'discounts-slider__navigation--disabled',
  },
  pagination: {
    el: '.discounts-slider__pagination',
    type: 'bullets',
  },
});

const fotoramaThumbnails = new Swiper('.fotorama__thumbnails', {
  spaceBetween: 10,
  slidesPerView: 'auto',
  watchSlidesProgress: true,
  slideToClickedSlide: true,
  centerInsufficientSlides: true,
});

// eslint-disable-next-line no-unused-vars
const fotorama = new Swiper('.fotorama__slider', {
  modules: [Thumbs, Pagination],
  spaceBetween: 0,
  thumbs: {
    swiper: fotoramaThumbnails,
  },
  pagination: {
    el: '.fotorama__pagination',
    type: 'bullets',
  },
  on: {
    slideChange: () => {
      const videoSlides = document.querySelectorAll('.fotorama__slide--video');
      videoSlides.forEach((video, index) => {
        const videoItem = video.querySelector('video');
        if (index !== fotorama.activeIndex) videoItem.pause();
      });
    },
  },
});

const countersCodeHead = `
<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?167",t.onload=function(){VK.Retargeting.Init("VK-RTRG-42241-gkrhc"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-42241-gkrhc" style="position:fixed; left:-999px;" alt=""/></noscript>

<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?162",t.onload=function(){VK.Retargeting.Init("VK-RTRG-429212-e2cle"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-429212-e2cle" style="position:fixed; left:-999px;" alt=""/></noscript>

  <script type="text/javascript">
  var __cs = __cs || [];
  __cs.push(["setCsAccount", "7o1bsgTuiKB6CwM_Gsw_3zQ3yXwqrZHx"]);
  __cs.push(["setCsHost", "//server.comagic.ru/comagic"]);
  </script>
  <script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>
  <script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=SXAFucnQQA46xpgnnVmEmJNt0OKyXp9rM*EExvfzVXxy0E0r0/kYXSNNKs8jBONYYpHitUxxTr0q5UnfONmMoQY2NzaHrm­oKqHGoGVaHaW9PUHIHJIue*S7yEau/EdOLrkQRCZKSdbg5PU4mJDF/ZJn4MpyxXp7cYapD1kKqtPc-&pixel_id=1000042241';</script>

  <link href="/bitrix/templates/.default/styles/vvs-button.css" rel="stylesheet" type="text/css" />
  <script src="http://www.vse-v-salon.ru/online-record/api/api.js" async type="text/javascript"></script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '596486391119483');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=596486391119483&ev=PageView&noscript=1" alt=""
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" data-skip-moving="true">
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
    ym(12906073, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript>
  <div><img src="https://mc.yandex.ru/watch/12906073" style="position:absolute; left:-9999px;" alt="" /></div>
</noscript>
<!-- /Yandex.Metrika counter -->
<script src="https://vk.com/js/api/videoplayer.js">
`;

const countersCodeBody = `
    <script type="text/javascript" src="https://w165949.yclients.com/widgetJS" charset="UTF-8"></script>
`;

counters(countersCodeHead, 'head');
counters(countersCodeBody);

const countdowns = document.querySelectorAll('.countdown');
countdowns.forEach((cd) => {
  const date = cd.dataset.date.split(',');
  // eslint-disable-next-line no-undef
  jQuery(cd).countdown({ timestamp: new Date(...date) });
});

appointment();
feedbackSliderInfo();
video360Init();
